# -*- coding: utf-8 -*-
"""
Created on Tue May 14 15:49:59 2019

@author: Rob
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go

# do imports
import os, shutil
import cv2
import numpy as np
from matplotlib import pyplot as plt
import scipy as sp
from scipy import interpolate
import pandas as pd



df = pd.DataFrame({
    'a': [1, 2, 3],
    'b': [4, 1, 4],
    'c': ['x', 'y', 'z'],
})


app = dash.Dash()
app.layout = html.Div([
    dcc.Dropdown(
        id='dropdown',
        options=[{'label': i, 'value': i} for i in df['c'].unique()],
        value='a'
    ),
    html.Div(id='output'),
])

@app.callback(Output('output', 'children'),
              [Input('dropdown', 'value')])
def update_output_1(value):
    # Safely reassign the filter to a new variable
    filtered_df = df[df['c'] == value]
    return len(filtered_df)


if __name__ == '__main__':
    app.run_server(debug=True)
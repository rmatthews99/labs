# -*- coding: utf-8 -*-
"""
Created on Tue May 14 15:49:59 2019

@author: Rob
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go

# do imports
import os, shutil
import cv2
import numpy as np
from matplotlib import pyplot as plt
import scipy as sp
from scipy import interpolate




# load up all the z slice data
# get the list of files
BASE_DIR = './data/Z-Slices/'
img_list = os.listdir(BASE_DIR)

# load a single image to get it's dimensions
im0 = cv2.imread(BASE_DIR+img_list[0])
# resize to make this code run faster at this point
resize_factor = 0.01
im0_rsz = cv2.cvtColor(cv2.resize(im0, (0, 0), fx=resize_factor, fy=resize_factor), cv2.COLOR_BGR2GRAY)

# create an array placeholder for this data
data_3d = np.zeros((im0_rsz.shape[0], im0_rsz.shape[1], len(img_list)), dtype='uint8')

# load the data into the array
for ii in range(len(img_list)):
    img = cv2.imread(BASE_DIR+img_list[ii])
    img = cv2.cvtColor(cv2.resize(img, (0, 0), fx=resize_factor, fy=resize_factor), cv2.COLOR_BGR2GRAY)
    data_3d[:,:,ii] = img



app = dash.Dash()
app.layout = html.Div(children=[
    html.H1(
        children='Tetrahive Technologies',
        style={
            'textAlign': 'center'
        }
    ),
    dcc.Input(id='depth-level', type='number', value=50),
    html.Button(id='submit-button', n_clicks=0, children='Submit'),
    html.Div(id='output-state')
]
)


@app.callback(Output('output-state', 'children'),
              [Input('submit-button', 'n_clicks')],
              [State('depth-level', 'value')])    
def generate_data(n_clicks=1, input1=54):    
    # now go in and see about doing an interpolation to 'cut' the data along a non-planar surface
    x_points = 3
    y_points = 3
    x_basis = np.linspace(0, 1, num=x_points)
    y_basis = np.linspace(0, 1, num=y_points)
    X_basis = x_basis*im0_rsz.shape[0]
    Y_basis = y_basis*im0_rsz.shape[1]
    Z_basis = [54]
    
    # create the mesh points
    X1, X2, X3 = np.meshgrid(X_basis, Y_basis, Z_basis)
    
    # edit only the mid-point for now
    X3[1][1] -= 0 #4
    
    # fit a spline
    bspln = interpolate.SmoothBivariateSpline(X1.flatten(), X2.flatten(), X3.flatten(), kx=x_points-1, ky=y_points-1)
    
    # get a meshgrid
    xx, yy = np.meshgrid(np.arange(im0_rsz.shape[0]), np.arange(im0_rsz.shape[1]))
    
    # evaluate the spline at all image points
    zz = bspln.ev(xx, yy)
    
    # interpolate the 3d-data at these points
    interp_pts = np.zeros((zz.size,3))
    interp_pts[:,0] = xx.flatten()
    interp_pts[:,1] = yy.flatten()
    interp_pts[:,2] = zz.flatten()
    zz2 = interpolate.interpn((np.arange(data_3d.shape[0]), 
                               np.arange(data_3d.shape[1]),
                               np.arange(data_3d.shape[2])), data_3d, interp_pts, method='linear')
                               
    
    zz2 = zz2.reshape(xx.shape).transpose()
    res = dcc.Graph(
                figure=go.Figure(
                    data=[go.Heatmap(z=generate_data())],
                ),
                style={'height': 600, 'width': 600},
                id='XRT0'
            )
    #return zz2
    return res


if __name__ == '__main__':
    app.run_server(debug=True)
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 15:49:59 2019

@author: Rob
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go

# do imports
import os, shutil
import cv2
import numpy as np
from matplotlib import pyplot as plt
import scipy as sp
from scipy import interpolate




# load up all the z slice data
# get the list of files
BASE_DIR = './data/Z-Slices/'
img_list = os.listdir(BASE_DIR)

# load a single image to get it's dimensions
im0 = cv2.imread(BASE_DIR+img_list[0])
# resize to make this code run faster at this point
resize_factor = 0.20
im0_rsz = cv2.cvtColor(cv2.resize(im0, (0, 0), fx=resize_factor, fy=resize_factor), cv2.COLOR_BGR2GRAY)

# create an array placeholder for this data
data_3d = np.zeros((im0_rsz.shape[0], im0_rsz.shape[1], len(img_list)), dtype='uint8')

# load the data into the array
for ii in range(len(img_list)):
    img = cv2.imread(BASE_DIR+img_list[ii])
    img = cv2.cvtColor(cv2.resize(img, (0, 0), fx=resize_factor, fy=resize_factor), cv2.COLOR_BGR2GRAY)
    data_3d[:,:,ii] = img

data_3d = np.fliplr(np.flipud(data_3d))

cmaps = ['Blackbody',
'Bluered',
'Blues',
'Earth',
'Electric',
'Greens',
'Greys',
'Hot',
'Jet',
'Picnic',
'Portland',
'Rainbow',
'RdBu',
'Reds',
'Viridis',
'YlGnBu',
'YlOrRd']

app = dash.Dash()
app.layout = html.Div(children=[
    html.H1(
        children='Tetrahive Technologies',
        style={
            'textAlign': 'center'
        }
    ),
    dcc.Slider(id='depth-level', min=0, max=data_3d.shape[-1], step=0.1, value=0),
    html.Div(id='slider-value', style={'margin-top': 5}),
    #dcc.Input(id='depth-level', type='number', value=50),
    html.P(),

    html.Div(id='depth-table'),
    html.Table([
    html.Tr([
        html.Td(dcc.Input(type='number', value=0, id='depth_{}_{}'.format(i, ii)), style={'padding-top':'2px', 'padding-bottom':'2px', 
        'text-align':'right'}) for ii in range(3)
        ]) for i in range(3)], style={'border':'1px', 'font-size':'1.2rem'}
        ),
    html.Div(id='output-state'),
    html.P(),
    html.Button(id='submit-button', n_clicks=0, children='Submit'),
    dcc.Graph(id='XRT0', style={'height': 600, 'width': 600},),
]
)



@app.callback(Output('slider-value', 'children'),
              [Input('depth-level', 'value')])
def display_value(value):
    return 'Depth Level: {}'.format(value)


@app.callback(Output('XRT0', 'figure'),
              [Input('submit-button', 'n_clicks')],
              [State('depth-level', 'value'),
                State('depth_0_0', 'value'), State('depth_0_1', 'value'), State('depth_0_2', 'value'),
                State('depth_1_0', 'value'), State('depth_1_1', 'value'), State('depth_1_2', 'value'),
                State('depth_2_0', 'value'), State('depth_2_1', 'value'), State('depth_2_2', 'value'),
                ])    
def generate_data(n_clicks, depth0, d00, d01, d02, d10, d11, d12, d20, d21, d22):    
    # now go in and see about doing an interpolation to 'cut' the data along a non-planar surface
    x_points = 3
    y_points = 3
    x_basis = np.linspace(0, 1, num=x_points)
    y_basis = np.linspace(0, 1, num=y_points)
    X_basis = x_basis*im0_rsz.shape[0]
    Y_basis = y_basis*im0_rsz.shape[1]
    Z_basis = [depth0]
    
    # create the mesh points
    X1, X2, X3 = np.meshgrid(X_basis, Y_basis, Z_basis)
    
    # edit only the mid-point for now
    X3[0][2] += d00
    X3[1][2] += d01
    X3[2][2] += d02
    X3[0][1] += d10
    X3[1][1] += d11
    X3[2][1] += d12
    X3[0][0] += d20
    X3[1][0] += d21
    X3[2][0] += d22
    
    # fit a spline
    bspln = interpolate.SmoothBivariateSpline(X1.flatten(), X2.flatten(), X3.flatten(), kx=x_points-1, ky=y_points-1)
    
    # get a meshgrid
    xx, yy = np.meshgrid(np.arange(im0_rsz.shape[0]), np.arange(im0_rsz.shape[1]))
    
    # evaluate the spline at all image points
    zz = bspln.ev(xx, yy)
    
    # interpolate the 3d-data at these points
    interp_pts = np.zeros((zz.size,3))
    interp_pts[:,0] = xx.flatten()
    interp_pts[:,1] = yy.flatten()
    interp_pts[:,2] = zz.flatten()
    zz2 = interpolate.interpn((np.arange(data_3d.shape[0]), 
                               np.arange(data_3d.shape[1]),
                               np.arange(data_3d.shape[2])), data_3d, interp_pts, method='linear')
                                   
    zz2 = zz2.reshape(xx.shape).transpose()
    #return zz2
    return go.Figure(data=[go.Heatmap(z=zz2, colorscale='Jet', zmin=0, zmax=256)],)




if __name__ == '__main__':
    app.run_server(debug=True)